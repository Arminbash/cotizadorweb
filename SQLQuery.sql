--create database CotizadorWeb
go
use CotizadorWeb
go
create table Employee(
	EmployeeId int primary key identity,
	EmployeeName varchar(300),
	Email varchar(100),
	Phone int,
	[Status] bit
)
go
create table UserRole(
	UserRoleId int primary key identity,
	UserRoleName varchar(20),
	[Status] bit
)
go
create table Users(
	UserId int primary key identity,
	UserRoleId int foreign key references UserRole(UserRoleId),
	UserName varchar(20),
	PasswordHash nvarchar(MAX),
	EmployeeId int foreign key references Employee(EmployeeId),
	[Status] bit
)
go
create table Tokens(
	TokenId int primary key identity,
	UserId int foreign key references Users(UserId),
	EntryDate datetime,
	DateExit datetime,
	Token nvarchar(MAX),
	Active bit
)
go
create table ProductCategory(
	ProductCategoryId int primary key identity,
	ProductCategoryName varchar(50),
	[Status] bit
)
go
create table ProductMark(
	ProductMarkId int primary key identity,
	ProductMarkName varchar(50),
	[Status] bit
)
go
create table Product(
	ProductId int primary key identity,
	ProductCode varchar(50),
	ProductName varchar(50),
	ProductCategoryId int foreign key references ProductCategory(ProductCategoryId),
	ProductMarkId int foreign key references ProductMark(ProductMarkId),
	ApplyIVA bit,
	[Status] bit
)
go
create table Kardex(
	KardexId int primary key identity,
	ProductId int foreign key references Product(ProductId),
	Input decimal(18,4),
	[OutPut] decimal(18,4),
	Cost decimal(18,4),
	DocumentId int,
	DocumentType varchar(20),
	[Status] bit
)
create table Providers(
	ProviderId int primary key identity,
	ProviderName varchar(50),
	ProviderEmail varchar(100),
	[Status] bit
)
go
create table Purchase(
	PurchaseId int primary key identity,
	UserId int foreign key references Users(UserId),
	PurchaseCode varchar(50),
	PurchaseDate date,
	ProviderId int foreign key references Providers(ProviderId),
	[Status] bit
)
go
create table PurchaseDetail(
	PurchaseDetailId int primary key identity,
	PurchaseId int foreign key references Purchase(PurchaseId),
	ProductId int foreign key references Product(ProductId),
	Quantity decimal(18,2),
	Price decimal(18,2),
	Discount decimal(18,2),
	IVA decimal(18,2),
	[Status] bit
)
go
create table Client(
	ClientId int primary key identity,
	ClientName varchar(20),
	ClientEmail varchar(100),
	[Status] bit
)
go
create table QuoteProduct(
	QuoteProductId int primary key identity,
	UserId int foreign key references Users(UserId),
	Number varchar(20),
	ClientId int foreign key references Client(ClientId),
	DateQuote date,
	[Status] bit
)
go
create table QuoteProductDetail(
	QuoteProductDetailId int primary key identity,
	QuoteProductId int foreign key references QuoteProduct(QuoteProductId),
	ProductId int foreign key references Product(ProductId),
	Quantity decimal(18,2),
	Price decimal(18,2),
	Discount decimal(18,2),
	IVA decimal(18,2),
	[Status] bit
)
go
create table Invoice(
	InvoiceId int primary key identity,
	UserId int foreign key references Users(UserId),
	Number varchar(20),
	ClientId int foreign key references Client(ClientId),
	DateQuote date,
	[Status] bit
)
go
create table InvoiceDetail(
	InvoiceDetailId int primary key identity,
	InvoiceId int foreign key references Invoice(InvoiceId),
	ProductId int foreign key references Product(ProductId),
    Quantity decimal(18,2),
	Price decimal(18,2),
	Discount decimal(18,2),
	IVA decimal(18,2),
	[Status] bit
)