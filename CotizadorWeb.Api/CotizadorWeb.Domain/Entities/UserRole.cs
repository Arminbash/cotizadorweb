﻿using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class UserRole
    {
        public UserRole()
        {
            Users = new HashSet<Users>();
        }
        public int UserRoleId { get; set; }
        public string UserRoleName { get; set; }
        public bool Status { get; set; }
        public ICollection<Users> Users { get; private set; }

    }
}
