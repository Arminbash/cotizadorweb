﻿using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class InvoiceDetail
    {
        public int InvoiceDetailId { get; set; }
        public int InvoiceId { get; set; }
        public int ProductId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public decimal IVA { get; set; }
        public bool Status { get; set; }

        public Invoice Invoice { get; set; }
        public Product Product { get; set; }
    }
}
