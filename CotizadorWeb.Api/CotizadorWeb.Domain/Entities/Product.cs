﻿using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class Product
    {
        public Product() 
        {
            Kardex = new HashSet<Kardex>();
            PurchaseDetail = new HashSet<PurchaseDetail>();
            QuoteProductDetail = new HashSet<QuoteProductDetail>();
            InvoiceDetail = new HashSet<InvoiceDetail>();
        }

        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductMarkId { get; set; }
        public bool ApplyIVA { get; set; }
        public bool Status { get; set; }

        public ProductCategory ProductCategory { get; set; }
        public ProductMark ProductMark { get; set; }

        public ICollection<Kardex> Kardex { get; private set; }
        public ICollection<PurchaseDetail> PurchaseDetail { get; private set; }
        public ICollection<QuoteProductDetail> QuoteProductDetail { get; private set; }
        public ICollection<InvoiceDetail> InvoiceDetail { get; private set; }

    }
}
