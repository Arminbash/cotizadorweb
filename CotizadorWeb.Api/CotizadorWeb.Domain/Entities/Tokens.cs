﻿using System;
using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class Tokens
    {
        public int TokenId { get; set; }
        public int UserId { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime DateExit { get; set; }
        public string Token { get; set; }
        public bool Active { get; set; }

        public Users Users { get; set; }
    }
}
