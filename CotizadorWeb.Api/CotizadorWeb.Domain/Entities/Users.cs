﻿using System;
using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class Users
    {
        public Users()
        {
            Tokens = new HashSet<Tokens>();
            Purchase = new HashSet<Purchase>();
            QuoteProduct = new HashSet<QuoteProduct>();
            Invoice = new HashSet<Invoice>();
        }
        public int UserId { get; set; }
        public int UserRoleId { get; set; }
        public String UserName { get; set; }
        public string PasswordHash { get; set; }
        public int EmployeeId { get; set; }
        public bool Status { get; set; }

        public UserRole userRole { get; set; }
        public Employee employee { get; set; }

        public ICollection<Tokens> Tokens { get; private set; }
        public ICollection<Purchase> Purchase { get; private set; }
        public ICollection<QuoteProduct> QuoteProduct { get; private set; }
        public ICollection<Invoice> Invoice { get; private set; }
    }
}
