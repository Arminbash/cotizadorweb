﻿using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class ProductMark
    {
        public ProductMark()
        {
            Product = new HashSet<Product>();
        }
        public int ProductMarkId { get; set; }
        public string ProductMarkName { get; set; }
        public bool Status { get; set; }
        public ICollection<Product> Product { get; private set; }
    }
}
