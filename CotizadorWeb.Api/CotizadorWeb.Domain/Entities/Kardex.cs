﻿using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class Kardex
    {
        public int KardexId { get; set; }
        public int ProductId { get; set; }
        public decimal Input { get; set; }
        public decimal OutPut { get; set; }
        public decimal Cost { get; set; }
        public int DocumentId { get; set; }
        public string DocumentType { get; set; }
        public bool Status { get; set; }

        public Product Product { get; set; }
    }
}
