﻿using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class PurchaseDetail
    {
        public int PurchaseDetailId { get; set; }
        public int PurchaseId { get; set; }
        public int ProductId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public decimal IVA { get; set; }
        public bool Status { get; set; }

        public Purchase Purchase { get; set; }
        public Product Product { get; set; }
    }
}
