﻿using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class ProductCategory
    {
        public ProductCategory()
        {
            Product = new HashSet<Product>();
        }
        public int ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; }
        public bool Status { get; set; }
        public ICollection<Product> Product { get; private set; }

    }
}
