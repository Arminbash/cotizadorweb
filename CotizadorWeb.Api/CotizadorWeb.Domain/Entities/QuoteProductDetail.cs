﻿using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class QuoteProductDetail
    {
        public int QuoteProductDetailId { get; set; }
        public int QuoteProductId { get; set; }
        public int ProductId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public bool Status { get; set; }

        public QuoteProduct QuoteProduct { get; set; }
        public Product Product { get; set; }
    }
}
