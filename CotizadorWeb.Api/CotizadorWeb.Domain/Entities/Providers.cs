﻿using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class Providers
    {
        public Providers()
        {
            Purchase = new HashSet<Purchase>();
        }
        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        public string ProviderEmail { get; set; }
        public bool Status { get; set; }
        public ICollection<Purchase> Purchase { get; private set; }

    }
}
