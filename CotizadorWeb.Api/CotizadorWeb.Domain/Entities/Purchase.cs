﻿using System;
using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class Purchase
    {
        public Purchase()
        {
            PurchaseDetail = new HashSet<PurchaseDetail>();
        }
        public int PurchaseId { get; set; }
        public int UserId { get; set; }
        public string PurchaseCode { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int ProviderId { get; set; }
        public bool Status { get; set; }

        public Users Users { get; set; }
        public Providers Providers { get; set; }

        public ICollection<PurchaseDetail> PurchaseDetail { get; private set; }
    }
}
