﻿using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class Client
    {
        public Client()
        {
            Invoice = new HashSet<Invoice>();
            QuoteProduct = new HashSet<QuoteProduct>();
        }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientEmail { get; set; }
        public bool Status { get; set; }

        public ICollection<Invoice> Invoice { get; private set; }
        public ICollection<QuoteProduct> QuoteProduct { get; private set; }
    }
}
