﻿using System;
using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class QuoteProduct
    {
        public QuoteProduct(){
            QuoteProductDetail = new HashSet<QuoteProductDetail>();
        }

        public int QuoteProductId { get; set; }
        public int UserId { get; set; }
        public String Number { get; set; }
        public int ClientId { get; set; }
        public DateTime DateQuote { get; set; }
        public bool Status { get; set; }

        public Users Users { get; set; }
        public Client client { get; set; }

        public ICollection<QuoteProductDetail> QuoteProductDetail { get; private set; }
    }
}
