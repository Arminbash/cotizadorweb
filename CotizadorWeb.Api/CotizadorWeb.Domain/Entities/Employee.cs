﻿using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class Employee
    {
        public Employee()
        {
            Users = new HashSet<Users>();
        }
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Email { get; set; }
        public int Phone { get; set; }
        public bool Status { get; set; }
        public ICollection<Users> Users { get; private set; }

    }
}
