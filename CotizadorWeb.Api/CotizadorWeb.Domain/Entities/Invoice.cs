﻿using System;
using System.Collections.Generic;

namespace CotizadorWeb.Domain.Entities
{
    public class Invoice
    {
        public Invoice()
        {
            InvoiceDetail = new HashSet<InvoiceDetail>();
        }
        public int InvoiceId { get; set; }
        public string Number { get; set; }
        public int ClientId { get; set; }
        public DateTime DateQuote { get; set; }
        public bool Status { get; set; }

        public Client Client { get; set; }

        public ICollection<InvoiceDetail> InvoiceDetail { get; private set; }
    }
}
