﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CotizadorWeb.Persistence.MSSQL
{
    public static class MsSqlServiceCollectionExtensions
    {
        public static IServiceCollection AddMssqlDbContext(
            this IServiceCollection serviceCollection,
            IConfiguration config = null)
        {
            serviceCollection.AddDbContext<GlobalDbContext, MsSqlGlobalDbContext>(options =>
            {
                options.UseSqlServer(config.GetConnectionString("GlobalDbContext"), b => b.MigrationsAssembly("CotizadorWeb.Persistence.MSSQL"));
            });
            return serviceCollection;
        }
    }
}
