﻿using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace CotizadorWeb.Persistence.MSSQL
{
    public class MsSqlContextFactory : IDesignTimeDbContextFactory<MsSqlGlobalDbContext>
    {
        public MsSqlGlobalDbContext CreateDbContext(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false)
                .AddJsonFile("appsettings.local.json", true)
                .Build();

            var builder = new DbContextOptionsBuilder<GlobalDbContext>();
            builder.UseSqlServer(
                config.GetConnectionString(nameof(GlobalDbContext)),
                b => b.MigrationsAssembly("CotizadorWeb.Persistence.MSSQL")
            );
            return new MsSqlGlobalDbContext(builder.Options);
        }
    }
}
