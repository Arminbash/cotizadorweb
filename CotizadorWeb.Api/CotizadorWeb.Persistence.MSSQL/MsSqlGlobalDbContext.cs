﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace CotizadorWeb.Persistence.MSSQL
{
    public class MsSqlGlobalDbContext : GlobalDbContext
    {
        public MsSqlGlobalDbContext(DbContextOptions options) : base(options)
        {
        }
    }
}
