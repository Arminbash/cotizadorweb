﻿using CotizadorWeb.Application.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CotizadorWeb.Persistence
{
    public class GlobalDbContext : DbContext, IGlobalDbContext
    {
        public GlobalDbContext()
        {

        }

        public GlobalDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(GlobalDbContext).Assembly);
        }
    }
}
