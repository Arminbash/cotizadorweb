﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CotizadorWeb.Domain.Entities;

namespace CotizadorWeb.Persistence.Configurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(e => e.ProductId);

            builder.HasOne(d => d.ProductCategory)
                .WithMany(p => p.Product)
                .HasForeignKey(d => d.ProductCategoryId)
                .HasConstraintName("FK_Product_ProductCategory");

            builder.HasOne(d => d.ProductMark)
                .WithMany(p => p.Product)
                .HasForeignKey(d => d.ProductMarkId)
                .HasConstraintName("FK_Product_ProductMark");

            builder.Property(e => e.Status)
                .HasColumnName("[Status]")
                .HasColumnType("bit");
        }
    }
}
