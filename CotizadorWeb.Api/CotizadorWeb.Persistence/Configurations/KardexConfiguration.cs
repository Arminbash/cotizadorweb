﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CotizadorWeb.Domain.Entities;

namespace CotizadorWeb.Persistence.Configurations
{
    public class KardexConfiguration : IEntityTypeConfiguration<Kardex>
    {
        public void Configure(EntityTypeBuilder<Kardex> builder)
        {
            builder.HasKey(e => e.KardexId);

            builder.HasOne(d => d.Product)
                .WithMany(p => p.Kardex)
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_Kardex_Product");

            builder.Property(e => e.Status)
                .HasColumnName("[Status]")
                .HasColumnType("bit");
        }
    }
}
