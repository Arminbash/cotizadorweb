﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CotizadorWeb.Domain.Entities;

namespace CotizadorWeb.Persistence.Configurations
{
    public class PurchaseConfiguration : IEntityTypeConfiguration<Purchase>
    {
        public void Configure(EntityTypeBuilder<Purchase> builder)
        {
            builder.HasKey(e => e.PurchaseId);

            builder.HasOne(d => d.Users)
                .WithMany(p => p.Purchase)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("FK_Purchase_Users");

            builder.HasOne(d => d.Providers)
                .WithMany(p => p.Purchase)
                .HasForeignKey(d => d.ProviderId)
                .HasConstraintName("FK_Purchase_Providers");

            builder.Property(e => e.Status)
                .HasColumnName("[Status]")  
                .HasColumnType("bit");
        }
    }
}
