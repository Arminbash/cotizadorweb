﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CotizadorWeb.Domain.Entities;

namespace CotizadorWeb.Persistence.Configurations
{
    public class ProductMarkConfiguration : IEntityTypeConfiguration<ProductMark>
    {
        public void Configure(EntityTypeBuilder<ProductMark> builder)
        {
            builder.HasKey(e => e.ProductMarkId);

            builder.Property(e => e.Status)
                .HasColumnName("[Status]")
                .HasColumnType("bit");
        }
    }
}
