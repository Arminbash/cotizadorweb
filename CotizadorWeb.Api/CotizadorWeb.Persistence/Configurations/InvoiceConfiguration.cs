﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CotizadorWeb.Domain.Entities;

namespace CotizadorWeb.Persistence.Configurations
{
    public class InvoiceConfiguration : IEntityTypeConfiguration<Invoice>
    {
        public void Configure(EntityTypeBuilder<Invoice> builder)
        {
            builder.HasKey(e => e.InvoiceId);

            builder.HasOne(d => d.Client)
               .WithMany(p => p.Invoice)
               .HasForeignKey(d => d.ClientId)
               .HasConstraintName("FK_Invoice_Client");

            builder.Property(e => e.Status)
                .HasColumnName("[Status]")
                .HasColumnType("bit");
        }
    }
}
