﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CotizadorWeb.Domain.Entities;

namespace CotizadorWeb.Persistence.Configurations
{
    public class QuoteProductConfiguration : IEntityTypeConfiguration<QuoteProduct>
    {
        public void Configure(EntityTypeBuilder<QuoteProduct> builder)
        {
            builder.HasKey(e => e.QuoteProductId);

            builder.HasOne(d => d.Users)
                .WithMany(p => p.QuoteProduct)
                .HasForeignKey(d => d.UserId)
                .HasConstraintName("FK_QuoteProduct_Users");

            builder.HasOne(d => d.client)
                .WithMany(p => p.QuoteProduct)
                .HasForeignKey(d => d.ClientId)
                .HasConstraintName("FK_QuoteProduct_Client");

            builder.Property(e => e.Status)
                .HasColumnName("[Status]")
                .HasColumnType("bit");
        }
    }
}
