﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CotizadorWeb.Domain.Entities;

namespace CotizadorWeb.Persistence.Configurations
{
    public class InvoiceDetailConfiguration : IEntityTypeConfiguration<InvoiceDetail>
    {
        public void Configure(EntityTypeBuilder<InvoiceDetail> builder)
        {
            builder.HasKey(e => e.InvoiceDetailId);

            builder.HasOne(d => d.Invoice)
                .WithMany(p => p.InvoiceDetail)
                .HasForeignKey(d => d.InvoiceId)
                .HasConstraintName("FK_InvoiceDetail_Invoice");

            builder.HasOne(d => d.Product)
                .WithMany(p => p.InvoiceDetail)
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_InvoiceDetail_Product");

            builder.Property(e => e.Status)
                .HasColumnName("[Status]")
                .HasColumnType("bit");
        }
    }
}
