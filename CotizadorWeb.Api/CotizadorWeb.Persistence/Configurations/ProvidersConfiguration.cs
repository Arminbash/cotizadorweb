﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CotizadorWeb.Domain.Entities;

namespace CotizadorWeb.Persistence.Configurations
{
    public class ProvidersConfiguration : IEntityTypeConfiguration<Providers>
    {
        public void Configure(EntityTypeBuilder<Providers> builder)
        {
            builder.HasKey(e => e.ProviderId);

            builder.Property(e => e.Status)
                .HasColumnName("[Status]")
                .HasColumnType("bit");
        }
    }
}
