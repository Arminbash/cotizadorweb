﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CotizadorWeb.Domain.Entities;

namespace CotizadorWeb.Persistence.Configurations
{
    public class QuoteProductDetailConfiguration : IEntityTypeConfiguration<QuoteProductDetail>
    {
        public void Configure(EntityTypeBuilder<QuoteProductDetail> builder)
        {
            builder.HasKey(e => e.QuoteProductDetailId);

            builder.HasOne(d => d.QuoteProduct)
                .WithMany(p => p.QuoteProductDetail)
                .HasForeignKey(d => d.QuoteProductId)
                .HasConstraintName("FK_QuoteProductDetail_QuoteProduct");

            builder.HasOne(d => d.Product)
                .WithMany(p => p.QuoteProductDetail)
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_QuoteProductDetail_Product");

            builder.Property(e => e.Status)
                .HasColumnName("[Status]")
                .HasColumnType("bit");
        }
    }
}
