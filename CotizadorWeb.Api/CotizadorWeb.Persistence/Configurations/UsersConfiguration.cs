﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CotizadorWeb.Domain.Entities;

namespace CotizadorWeb.Persistence.Configurations
{
    public class UsersConfiguration : IEntityTypeConfiguration<Users>
    {
        public void Configure(EntityTypeBuilder<Users> builder)
        {
            builder.HasKey(e => e.UserId);

            builder.HasOne(d => d.employee)
               .WithMany(p => p.Users)
               .HasForeignKey(d => d.EmployeeId)
               .HasConstraintName("FK_Users_Employee");

            builder.HasOne(d => d.userRole)
                .WithMany(p => p.Users)
                .HasForeignKey(d => d.UserRoleId)
                .HasConstraintName("FK_Users_UserRole");

            builder.Property(e => e.Status)
               .HasColumnName("[Status]")
               .HasColumnType("bit");
        }
    }
}
