﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CotizadorWeb.Domain.Entities;

namespace CotizadorWeb.Persistence.Configurations
{
    public class PurchaseDetailConfiguration : IEntityTypeConfiguration<PurchaseDetail>
    {
        public void Configure(EntityTypeBuilder<PurchaseDetail> builder)
        {
            builder.HasKey(e => e.PurchaseDetailId);

            builder.HasOne(d => d.Purchase)
                .WithMany(p => p.PurchaseDetail)
                .HasForeignKey(d => d.PurchaseId)
                .HasConstraintName("FK_PurchaseDetail_Purchase");

            builder.HasOne(d => d.Product)
                .WithMany(p => p.PurchaseDetail)
                .HasForeignKey(d => d.ProductId)
                .HasConstraintName("FK_PurchaseDetail_Product");

            builder.Property(e => e.Status)
                .HasColumnName("[Status]")
                .HasColumnType("bit");
        }
    }
}
